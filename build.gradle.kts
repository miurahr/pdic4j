import java.io.FileInputStream
import java.util.Properties

plugins {
    checkstyle
    jacoco
    signing
    groovy
    `java-library`
    `java-library-distribution`
    `maven-publish`
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.nexus.publish)
    alias(libs.plugins.sphinx)
}

group = "tokyo.northside"

val dotgit = project.file(".git")
var versionDescribe: String = ""
if (dotgit.exists()) {
    val exe = providers.exec {
        setIgnoreExitValue(true)
        setCommandLine("git", "describe", "--tags", "--always", "--first-parent", "--abbrev=7", "--match=v*", "HEAD")
    }
    versionDescribe = when {
        exe.result.get().exitValue.equals(0) -> exe.standardOutput.asText.get().trim()
        else -> "v0.1.0-SNAPSHOT"  // when git error
    }
} else {
    val gitArchival = project.file(".git-archival.properties")
    val props = Properties()
    props.load(FileInputStream(gitArchival))
    versionDescribe = props.getProperty("describe").toString()
}

val cleanVersionRegex = "^v\\d+\\.\\d+(\\.\\d+)?$".toRegex()
val snapshotVersionRegex = "^v\\d+\\.\\d+.*-SNAPSHOT$".toRegex()
version = when {
    cleanVersionRegex.matches(versionDescribe) -> versionDescribe.substring(1)
    snapshotVersionRegex.matches(versionDescribe) -> versionDescribe
    else -> versionDescribe.substring(1) + "-SNAPSHOT"
}

tasks.register("printVersion") {
    doLast {
        println(project.version)
    }
    group = "verification"
}

tasks.updateDaemonJvm {
    jvmVersion = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.annotations)
    implementation(libs.commons.lang3)
    implementation(libs.commons.io)
    implementation(libs.icj4j.charset)
    testImplementation(libs.junit.jupiter)
    testImplementation(libs.groovy.all)
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
    withSourcesJar()
    withJavadocJar()
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

spotbugs {
    excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    tasks.spotbugsMain {
        reports.create("html") {
            required.set(true)
        }
    }
    tasks.spotbugsTest {
        reports.create("html") {
            required.set(true)
        }
    }
}

jacoco {
    toolVersion="0.8.6"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        xml.required.set(false)
        html.required.set(true)
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            pom {
                name.set("pdic4j")
                description.set("PDIC access library for java")
                url.set("https://codeberg.org/miurahr/pdic4j")
                licenses {
                    license {
                        name.set("The GNU General Public License, Version 3")
                        url.set("https://www.gnu.org/licenses/licenses/gpl-3.html")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("miurahr")
                        name.set("Hiroshi Miura")
                        email.set("miurahr@linux.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://codeberg.org/miurahr/pdic4j.git")
                    developerConnection.set("scm:git:git://codeberg.org/miurahr/pdic4j.git")
                    url.set("https://codeberg.org/miurahr/pdic4j")
                }
            }
        }
    }
}

val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
tasks.withType<Sign> {
    onlyIf { !version.toString().endsWith("-SNAPSHOT") && (signKey != null) }
}

signing {
    when (signKey) {
        "signingKey" -> {
            val signingKey: String? by project
            val signingPassword: String? by project
            useInMemoryPgpKeys(signingKey, signingPassword)
        }
        "signing.keyId" -> {/* do nothing */}
        "signing.gnupg.keyName" -> {
            useGpgCmd()
        }
    }
    sign(publishing.publications["mavenJava"])
}

// ---------- publish to sonatype OSSRH
val sonatypeUsername: String? by project
val sonatypePassword: String? by project
nexusPublishing.repositories {
    sonatype {
        if (sonatypeUsername != null && sonatypePassword != null) {
            username.set(sonatypeUsername)
            password.set(sonatypePassword)
        } else {
            username.set(System.getenv("SONATYPE_USER"))
            password.set(System.getenv("SONATYPE_PASS"))
        }
    }
}

tasks.sphinx {
    group = "documentation"
    sourceDirectory {"docs"}
}

tasks.jacocoTestCoverageVerification {
    dependsOn(tasks.test)
    violationRules {
        rule {
            element = "PACKAGE"
            includes = listOf("**.pdic")
            limit {
                minimum = "0.80".toBigDecimal()
            }
        }
    }
}

spotless {
    setEnforceCheck(false)
    format("misc") {
        target(listOf("*.gradle", ".gitignore", "*.rst"))
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    java {
        target(listOf("src/*/java/**/*.java"))
        removeUnusedImports()
        importOrder()
        palantirJavaFormat()
    }
}
