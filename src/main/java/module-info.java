module tokyo.northside.pdic4j {
    exports tokyo.northside.pdic;

    requires org.apache.commons.io;
    requires org.apache.commons.lang3;
    requires org.jetbrains.annotations;
    requires com.ibm.icu;
    requires com.ibm.icu.charset;
}
