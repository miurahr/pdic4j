/*
 * PDIC4j, a PDIC dictionary access library.
 * Copyright (C) 2021,2024 Hiroshi Miura.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.pdic;

import com.ibm.icu.charset.CharsetICU;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author wak (Apache-2.0)
 * @author Hiroshi Miura
 */
@SuppressWarnings("avoidInlineConditionals")
class DictionaryData {

    private static final int SECTOR_SIZE = 0x200;

    private final List<PdicElement> searchResults = new ArrayList<>();
    private final long start;
    private final long size;
    private final boolean blockBitsInt;
    private final long nIndex;
    private final long blocksize;
    private final AnalyzeBlock analyze;
    private final RandomAccessFile sourceStream;
    private final IndexCache indexCache;

    private boolean match;
    private int searchmax; // 最大検索件数

    private long bodyPtr;
    private int[] indexPtr;
    private int lastIndex = 0;
    private boolean loaded = false;


    DictionaryData(@NotNull final RandomAccessFile file, @NotNull final PdicHeader header, final int searchMax) {
        start = header.getDataStart();
        size = header.getDataSize();
        nIndex = header.getNindex2();
        blockBitsInt = header.isIndexBlkbit();
        blocksize = header.getBlockSize();
        this.searchmax = searchMax;
        sourceStream = file;
        indexCache = new IndexCache(sourceStream, start, size);
        analyze = new AnalyzeBlock();
        indexPtr = new int[((int) nIndex + 1)]; // fixme
    }

    /**
     * インデックス領域を検索.
     *
     * @param word to search.
     * @return index of block
     */
    long searchIndexBlock(@NotNull final String word) {
        if (!loaded) {
            return 0;
        }
        long min = 0;
        long max = nIndex - 1;

        ByteBuffer buffer = Utils.encodetoByteBuffer(CharsetICU.forNameICU("BOCU-1"), word);
        int limit = buffer.limit();
        byte[] bytes = new byte[limit];
        System.arraycopy(buffer.array(), 0, bytes, 0, limit);
        for (int i = 0; i < 32; i++) {
            if ((max - min) <= 1) {
                return min;
            }
            final int look = (int) ((min + max) / 2);
            final int len = indexPtr[look + 1] - indexPtr[look] - (blockBitsInt ? 4 : 2);
            final int comp = indexCache.compare(bytes, 0, bytes.length, indexPtr[look], len);
            if (comp < 0) {
                max = look;
            } else if (comp > 0) {
                min = look;
            } else {
                return look;
            }
        }
        return min;
    }

    /**
     * Read index blocks.
     *
     * @param indexCacheFile cache file to store index data.
     * @return true when successfully read block, otherwise false.
     */
    boolean readIndexBlock(@Nullable final File indexCacheFile) throws IOException {
        bodyPtr = start + size;
        loaded = getIndexFromCache(indexCacheFile) || createIndexAndWriteCache(indexCacheFile);
        return loaded;
    }

    private boolean getIndexFromCache(@Nullable final File indexCacheFile) throws IOException {
        if (indexCacheFile != null && indexCacheFile.isFile()) {
            try (FileInputStream fis = new FileInputStream(indexCacheFile)) {
                ReadableByteChannel channel = Channels.newChannel(fis);
                ByteBuffer byteBuffer = ByteBuffer.allocate(SECTOR_SIZE);
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                int index = 0;
                while (channel.read(byteBuffer) != -1) {
                    byteBuffer.flip();
                    IntBuffer buffer = byteBuffer.asIntBuffer();
                    while (buffer.hasRemaining() && index < nIndex) {
                        indexPtr[index++] = buffer.get();
                    }
                    if (index >= nIndex) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void writeCache(@NotNull final File indexCacheFile) throws IOException {
        final int nindex = (int) nIndex; // fixme.
        try (FileOutputStream fos = FileUtils.openOutputStream(indexCacheFile)) {
            WritableByteChannel channel = Channels.newChannel(fos);
            ByteBuffer byteBuffer = ByteBuffer.allocate(SECTOR_SIZE);
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            int c = 0;
            while (c < nindex) {
                IntBuffer buffer = byteBuffer.asIntBuffer();
                while (buffer.hasRemaining() && c < nindex) {
                    buffer.put(indexPtr[c++]);
                }
                channel.write(byteBuffer);
                byteBuffer.flip();
            }
        }
    }

    private boolean createIndexAndWriteCache(@Nullable final File indexCacheFile) throws IOException {
        if (indexCache.createIndex((blockBitsInt? 4 : 2), nIndex, indexPtr)) {
            if (indexCacheFile != null) {
                writeCache(indexCacheFile);
            }
            return true;
        }
        indexPtr = null;
        return false;
    }

    /**
     * num個目の見出し語の実体が入っているブロック番号を返す.
     *
     * @param num index number.
     * @return physical block number.
     */
    int getBlockNo(final int num) {
        int blkptr = indexPtr[num] - (blockBitsInt? 4 : 2);
        lastIndex = num;
        if (blockBitsInt) {
            return indexCache.getInt(blkptr);
        } else {
            return indexCache.getShort(blkptr);
        }
    }

    boolean hasExactMatch() {
        return match;
    }

    /**
     * Get a maximum number of search results.
     * @return a maximum number of entries.
     */
    int getSearchMax() {
        return searchmax;
    }

    /**
     * Set maximum entries of search results.
     * @param m maximum number of entries.
     */
    void setSearchMax(final int m) {
        searchmax = m;
    }

    /**
     * Enter a search job with word.
     *
     * @param word keyword to search.
     * @return true when entry found, otherwise false.
     * @throws IOException when file access error happened.
     */
    boolean searchWord(@NotNull final String word) throws IOException {
        // 検索結果クリア
        int cnt = 0;
        searchResults.clear();

        long ret = searchIndexBlock(word);
        match = false;
        boolean searchret = false;
        while (true) {
            // 最終ブロックは超えない
            if (ret < nIndex) {
                // 該当ブロック読み出し
                int block = getBlockNo((int) ret++);
                byte[] pblk = readBlockData(block);
                if (pblk != null) {
                    analyze.setBuffer(pblk);
                    analyze.setSearch(word);
                    searchret = analyze.searchWord();
                    // 未発見でEOBの時のみもう一回、回る
                    if (!searchret && analyze.isEob()) {
                        continue;
                    }
                }
            }
            // 基本一回で抜ける
            break;
        }
        if (searchret) {
            // 前方一致するものだけ結果に入れる
            do {
                PdicElement res = analyze.getRecord();
                if (res == null) {
                    break;
                }
                // 完全一致するかチェック
                if (res.getIndexWord().compareTo(word) == 0) {
                    match = true;
                }
                searchResults.add(res);

                cnt++;
                // 取得最大件数超えたら打ち切り
            } while (cnt < searchmax && hasMoreResult(true));
        }
        return searchret;
    }

    /**
     * Get results of last search.
     * @return search results.
     */
    List<PdicElement> getResult() {
        return searchResults;
    }

    @SuppressWarnings("unused")
    List<PdicElement> getMoreResult() throws IOException {
        searchResults.clear();
        int cnt = 0;
        // 前方一致するものだけ結果に入れる
        while (cnt < searchmax && hasMoreResult(true)) {
            PdicElement res = analyze.getRecord();
            if (res == null) {
                break;
            }
            searchResults.add(res);
            cnt++;
        }
        return searchResults;
    }

    boolean hasMoreResult(final boolean incrementptr) throws IOException {
        boolean result = analyze.hasMoreResult(incrementptr);
        if (!result) {
            if (analyze.isEob()) { // EOBなら次のブロック読み出し
                int nextindex = lastIndex + 1;
                // 最終ブロックは超えない
                if (nextindex < nIndex) {
                    int block = getBlockNo(nextindex);

                    // 該当ブロック読み出し
                    byte[] pblk = readBlockData(block);

                    if (pblk != null) {
                        analyze.setBuffer(pblk);
                        result = analyze.hasMoreResult(incrementptr);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Read data blocks.
     *
     * @param blkno block number to seek when read.
     * @return data block read.
     * @throws IOException when read error happened.
     */
    byte[] readBlockData(final long blkno) throws IOException {
        byte[] buff = new byte[SECTOR_SIZE];
        byte[] pbuf = buff;
        sourceStream.seek(bodyPtr + blkno * blocksize);
        if (sourceStream.read(pbuf, 0, SECTOR_SIZE) < 0) {
            return null;
        }

        // length of block.
        int len = ((int) (pbuf[0])) & 0xFF;
        len |= (((int) (pbuf[1])) & 0xFF) << 8;

        // ブロック長判定
        if ((len & 0x8000) != 0) { // 32bit
            len &= 0x7FFF;
        }
        if (len > 0) {
            // ブロック不足分読込
            if (len * blocksize > 0x200) {
                pbuf = new byte[(int) (blocksize * len)];
                System.arraycopy(buff, 0, pbuf, 0, SECTOR_SIZE);
                if (sourceStream.read(pbuf, SECTOR_SIZE, (int) (len * blocksize - SECTOR_SIZE)) < 0) {
                    return null;
                }
            }
        } else {
            pbuf = null;
        }
        return pbuf;
    }
}
