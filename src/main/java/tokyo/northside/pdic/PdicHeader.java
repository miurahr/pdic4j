/*
 * PDIC4j, a PDIC dictionary access library.
 * Copyright (C) 2021,2024 Hiroshi Miura.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.pdic;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

/**
 * @author wak (Apache-2.0)
 * @author Hiroshi Miura
 */
@SuppressWarnings({"visibilitymodifier", "membername"})
final class PdicHeader {
    private static final int L_HEADERNAME = 100; //   ヘッダー部文字列長
    private static final int L_DICTITLE = 40; //   辞書タイトル名長

    private final String headername; //   辞書ヘッダータイトル
    private final String dictitle; //   辞書名
    private final short version; //   辞書のバージョン
    private final int lword; //   見出語の最大長 ushort
    private final int ljapa; //   訳語の最大長 ushort
    private final short blockSize; //  (256 ) １ブロックのバイト数 固定
    private final int indexBlock; //   インデックスブロック数 ushort
    private final short headerSize; //   ヘッダーのバイト数 - fixed 1024
    // private final short indexSize;  //  ( ) インデックスのバイト数 未使用

    // private short nindex;       //  ( ) インデックスの要素の数 未使用
    // private short nblock;       //  ( ) 使用データブロック数 未使用
    // private int nword;    //   登録単語数

    // private byte dicorder;      //   辞書の順番
    // private byte dictype;       //   辞書の種別

    private final byte os; // OS
    private final boolean indexBlkbit; // false:16bit, true:32bit
    private final long extheader; //   拡張ヘッダーサイズ
    private final long nindex2; //   インデックス要素の数 ulong
    // public final long nblock2;       //   使用データブロック数 ulong

    // public int update_count;    //   辞書更新回数
    // public String dicident;      //   辞書識別子

    private PdicHeader(Builder builder) {
        this.headername = builder.headername;
        this.dictitle = builder.dictitle;
        this.lword = builder.lword;
        this.ljapa = builder.ljapa;
        this.version = builder.version;
        this.blockSize = builder.blockSize;
        this.indexBlock = builder.indexBlock;
        this.headerSize = builder.headerSize;
        this.os = builder.os;
        this.indexBlkbit = builder.indexBlkbit;
        this.extheader = builder.extheader;
        this.nindex2 = builder.nindex2;
    }

    @SuppressWarnings("unused")
    public String getHeaderName() {
        return headername;
    }

    @SuppressWarnings("unused")
    public String getDicTitle() {
        return dictitle;
    }

    @SuppressWarnings("unused")
    public int getLword() {
        return lword;
    }

    @SuppressWarnings("unused")
    public int getLjapa() {
        return ljapa;
    }

    public short getVersion() {
        return version;
    }

    public short getBlockSize() {
        return blockSize;
    }

    @SuppressWarnings("unused")
    public int getIndexBlock() {
        return indexBlock;
    }

    @SuppressWarnings("unused")
    public short getHeaderSize() {
        return headerSize;
    }

    public byte getOs() {
        return os;
    }

    public boolean isIndexBlkbit() {
        return indexBlkbit;
    }

    @SuppressWarnings("unused")
    public long getExtheader() {
        return extheader;
    }

    public long getNindex2() {
        return nindex2;
    }

    public long getDataStart() {
        return headerSize + extheader;
    }

    public long getDataSize() {
        return (long) blockSize * indexBlock;
    }

    /**
     * Header loader.
     *
     * @param headerBlock data block of header.
     * @return PdicHeader object.
     */
    public static PdicHeader load(final ByteBuffer headerBlock) throws PdicException {
        Builder builder = new Builder();
        Charset sjisset = Charset.forName("X-SJIS");

        byte[] headernamebuff = new byte[L_HEADERNAME];
        byte[] dictitlebuff = new byte[L_DICTITLE];

        headerBlock.flip();
        headerBlock.order(ByteOrder.LITTLE_ENDIAN);
        headerBlock.get(headernamebuff);
        builder.headername = sjisset.decode(ByteBuffer.wrap(headernamebuff)).toString();
        headerBlock.get(dictitlebuff);
        builder.dictitle = sjisset.decode(ByteBuffer.wrap(dictitlebuff)).toString();
        short version = headerBlock.getShort();

        if ((version & 0xFF00) != 0x0500 && (version & 0xFF00) != 0x0600) {
            // Support version 5.x and 6.x only.
            throw new PdicException("Unsupported dictionary version.");
        }
        builder.version = version;

        builder.lword = Short.toUnsignedInt(headerBlock.getShort());
        builder.ljapa = Short.toUnsignedInt(headerBlock.getShort());

        builder.blockSize = headerBlock.getShort();
        builder.indexBlock = Short.toUnsignedInt(headerBlock.getShort());
        builder.headerSize = headerBlock.getShort(); // fixed to 1024
        headerBlock.getShort(); // index_size - Unused
        headerBlock.getShort(); // empty_block - Unused
        headerBlock.getShort(); // nindex - Unused
        headerBlock.getShort(); // nblock - Unused

        headerBlock.getInt(); // nword

        headerBlock.get(); // dicorder
        headerBlock.get(); // dictype
        //   単語属性の長さ
        byte attrlen = headerBlock.get();
        builder.os = headerBlock.get();

        headerBlock.getInt(); // ole_number

        // lid_dummy
        headerBlock.getShort();
        headerBlock.getShort();
        headerBlock.getShort();
        headerBlock.getShort();
        headerBlock.getShort();

        builder.indexBlkbit = (headerBlock.get() != 0);
        headerBlock.get(); // dummy0
        builder.extheader = Integer.toUnsignedLong(headerBlock.getInt());
        headerBlock.getInt(); // empty_block2
        builder.nindex2 = Integer.toUnsignedLong(headerBlock.getInt());
        headerBlock.getInt(); // nblock2

        // 固定部分チェック
        if (attrlen != 1) {
            throw new PdicException("Invalid dictionary.");
        }
        return new PdicHeader(builder);
    }

    static class Builder {
        private String headername;
        private String dictitle;
        private short version;
        private int lword;
        private int ljapa;
        private short blockSize;
        private int indexBlock;
        private short headerSize;
        private byte os;
        private boolean indexBlkbit;
        private long extheader;
        private long nindex2;
    }
}
