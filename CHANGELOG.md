# Change Log

All notable changes to this project will be documented in this file.

## [Unreleased]

## [v0.7.1]

- Improve CI/CD

## [v0.7.0]

### Important Notice
- Change package to tokyo.northside.pdic

### Changes
- docs: add usage section
- Bump Groovy@4.0.23
- Bump commons-io@2.17.0
- Bump commons-lang3@3.14.0
- chore: fix configuration of the ossrh authetication

## [v0.6.0]

- Fix: check load status when search action
- Fix: avoid WeakHashMap for the cache
- Refactor header parser
- Fix: use long/int for unsigned int/short
- Bump Gradle@8.8
- Bump Spotbugs@6.0.13
- Bump Spotless@6.25.0
- Configure Gradle daemon JVM as Java17

## [v0.5.0]

- Bump requirement to java 11
- Add module-info.java

## [v0.4.2]

### Fixed

- Gredle: Fix group id.

## [v0.4.1]

### Fixed

- Gradle: key signatory configuration

### Changed

- Bump gradle@7.3.3

## [v0.4.0]

### Changed

- Migrate forge site to codeberg.org.
- Link with azure-pipelines for CI.
- Drop some github related files.
- Drop coveralls badge.
- Publish documents on readthedocs.org
- Gradle: produce version.properties
- Gradle: accept secring.gpg for signing.
- Docs: set language="ja"
- Docs: add .readthedocs.yaml configuration

## [v0.3.3]

### Fixed

- getEntries and getEntriesPredictive works correctly.
  - Past versions returns prefix search result for getEntries and null for getEntriesPredictive

### Changed

- rewrite test with groovy and drop dependency for kotlin library

## [v0.3.2]

### Fixed

- Exception when cache file does not exist

## [v0.3.1]

### Fixed

- Fix refactoring error in AnalyzeBlock class

### Added

- External data test case that run when data exists
- Add badges on README
- Add install instruction on README

### Changed

- Change PdicElement.PdicElementBuilder setter method names
- Test cases for coverage improvements:
  - Attribution field
  - RTL language
  - Cache file creation

## [v0.3.0]

- Change public API
- Update documents
- Change class names and refactoring.

## [v0.2.0]

- Fix public APIs
- Add spec docs
- Publish github pages
- Publish javadoc to github pages
- Publish coveralls coverage
- Add test cases

## v0.1.0

- First internal release

[Unreleased]: https://codeberg.org/miurahr/pdic4j/compare/v0.7.1...HEAD
[v0.7.1]: https://codeberg.org/miurahr/pdic4j/compare/v0.7.0...v0.7.1
[v0.7.0]: https://codeberg.org/miurahr/pdic4j/compare/v0.6.0...v0.7.0
[v0.6.0]: https://codeberg.org/miurahr/pdic4j/compare/v0.5.0...v0.6.0
[v0.5.0]: https://codeberg.org/miurahr/pdic4j/compare/v0.4.2...v0.5.0
[v0.4.2]: https://codeberg.org/miurahr/pdic4j/compare/v0.4.1...v0.4.2
[v0.4.1]: https://codeberg.org/miurahr/pdic4j/compare/v0.4.0...v0.4.1
[v0.4.0]: https://codeberg.org/miurahr/pdic4j/compare/v0.3.3...v0.4.0
[v0.3.3]: https://codeberg.org/miurahr/pdic4j/compare/v0.3.2...v0.3.3
[v0.3.2]: https://codeberg.org/miurahr/pdic4j/compare/v0.3.1...v0.3.2
[v0.3.1]: https://codeberg.org/miurahr/pdic4j/compare/v0.3.0...v0.3.1
[v0.3.0]: https://codeberg.org/miurahr/pdic4j/compare/v0.2.0...v0.3.0
[v0.2.0]: https://codeberg.org/miurahr/pdic4j/compare/v0.1.0...v0.2.0
