Usage
=====

You can create ``PdicDictionary`` object using ``loadDictionary`` function.

.. code-block:: java

    import tokyo.northside.pdic.PdicDictionary

    var file = new File("Sample.dic");
    var cache = new File("Sample.idx");
    PdicDictionary dictionary = PdicDictionary.loadDictionary(file, cache);


then you can get dictionary entries with ``getEntries`` method

.. code-block:: java

    for (var ele: dictionary.getEntries("hello")) {
        println(ele.headword);
        println(ele.translation);
        println(ele.example);
    }


``PdicElement`` class has fields.

.. csv-table:: Fields of PdicElement
   :header: "name", "type", "description"

    attribute, byte, "attribute bit field"
    indexWord, String, "keyword for indexing, ex. index"
    headWord, String, "title word to show, ex. Indexes"
    translation, String, "translation explanation"
    example, String, "example of word usage"
    pronunciation, String, "pronunciation explanation"


Index file
----------

pdic4j create the file of index as like ``name.dic.idx``.
It stores byte positions of each entries against indexWord in the original binary format.
Pdic4j caches the index data of searched elements read from the index file with the simple LRU algorithm.
