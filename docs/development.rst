Development note
================

When you read a specification of pdic format, please care for data type of fields.
The specification is written for C/C++/C# implementer on a X86 windows.
Because Java don't have unsigned types, so please take care.
Here is a comparison between C/C++/C# on windows and Java

.. csv-table:: a comparison of data types
   :header: "Type", "Windows", "Java"

    byte,         8,       8
    short,        16,      16
    ushort,       16,      N.A.
    int,          32,      32
    uint,         32,      N.A.
    long,         32,      64
    ulong,        32,      N.A.
    longlong,     64,      N.A.


We use long for both uint and ulong, and int for ushort.
