pdic4j
======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   usage
   development
   spec/README
   spec/pdicu-dic

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
