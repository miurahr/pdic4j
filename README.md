# PDIC4j library

[![Check status](https://ci.codeberg.org/api/badges/12813/status.svg)](https://ci.codeberg.org/repos/12813/branches/main)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/tokyo.northside/pdic4j/badge.svg?style=plastic)](https://search.maven.org/artifact/tokyo.northside/pdic4j)
[![LiberaPay](https://img.shields.io/liberapay/patrons/miurahr.svg?logo=liberapay)](https://liberapay.com/miurahr/donate)

PDIC is one of the popular dictionary formats.
PDIC4j is a parser library for PDIC/Unicode dictionary.

## Development status

A status of library development is considered as `Beta`.

## Document

- https://pdic4j.readthedocs.io/ja/stable/

## Install

### Apache Maven

<details>

```xml
<dependency>
  <groupId>tokyo.northside</groupId>
  <artifactId>pdic4j</artifactId>
  <version>0.6.0</version>
</dependency>
```

</details>

### Gradle Groovy DSL

<details>

```groovy
implementation 'tokyo.northside:pdic4j:0.6.0'
```

</details>

### Gradle kotlin DSL

<details>

```kotlin
implementation("tokyo.northside:pdic4j:0.6.0")
```

</details>

### Scala SBT

<details>

```
libraryDependencies += "tokyo.northside" % "pdic4j" % "0.6.0"
```

</details>

## License

PDIC4J is distributed under GNU General Public License version 3 or (at your option) any later version.


[![Donate using LibraPay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/miurahr/donate)
